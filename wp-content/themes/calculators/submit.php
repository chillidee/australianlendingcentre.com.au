<?php

if (!defined('__DIR__')) {
  define('__DIR__', dirname(__FILE__));
}

if (isset($_GET['data']) && isset($_GET['update'])) {
	
  $data = json_decode($_GET['data']);
  $update = json_decode($_GET['update']);

  if (count($data) > 0) {
	  	
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= "From: Australian Lending Centre <info@australianlendingcentre.com.au>\r\n"."X-Mailer: php";	
	  
	$message = '<html><body>';
	$comments = '';
	
	$firstName = '';
	$mobile = '';
	$email = '';
	
    foreach ($data as $row) {
      if ($row->key === 'SEPARATOR') {
        $message .= '<br/>';
		$comments .= '<br>';		
      } else {        
		$message .= '<span>' . ucfirst(trim($row->key, ':')) . ': ' . $row->value . '</span>';      		  
		$message .= '<br/>';		
		if(strtoupper(trim($row->key, ':')) == 'FIRST NAME') {
		  $firstName = $row->value;
		} else if(strtoupper(trim($row->key, ':')) == 'MOBILE') {
		  $mobile = $row->value;
		} else if(strtoupper(trim($row->key, ':')) == 'EMAIL') {
		  $email = $row->value;
		} else if (strtoupper($row->key) == 'CUSTOMER') {
		  // Skip
		} else {
		  $comments .= ucfirst(trim($row->key, ':')) . ': ' . $row->value;    
		  $comments .= '<br>';
		}
      }
    }
	
	$message .= '</body></html>';
	
	//mail('reception@australianlendingcentre.com.au', 'Budget Calculator' , $message, $headers);
	mail('boni@chillidee.com.au', 'Budget Calculator' , $message, $headers);  
		
	if($update == 0) {
		
		// Insert Lead
	
		$platform = 'desktop';
		$useragent = $_SERVER['HTTP_USER_AGENT'];
		if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4)))
			$platform = 'mobile';
		
		$lead = array();
		$lead['cid'] = 'alc';
		$lead['title'] = '';
		$lead['firstName'] = $firstName;
		$lead['lastName'] = '[UNKNOWN]';
		$lead['loanAmount'] = 0;
		$lead['typeOfLoan'] = 158;
		$lead['hasProperty'] = 'false';
		$lead['haveDeposit'] = 'false';
		$lead['realEstateValue'] =  0;
		$lead['balanceOwing'] = 0;
		$lead['mobileNumber'] = $mobile;
		$lead['landLineNumber'] = '';
		$lead['emailAddress'] = $email;
		$lead['suburb'] = ' ';
		$lead['state'] = 'NSW';
		$lead['postCode'] = '0000';		
		$lead['referral'] = '';			
		$lead['comments'] = 'Customer has just started filling out the Budget Calculator.';
		$lead['referrer'] = $_SERVER['HTTP_REFERER'];
		$data['t'] = null;
		$data['k'] = null;
		$data['a'] = null;
		$lead['platform'] = $platform;
		$lead['UserHasDefaults'] = '';
		$lead['UserLoanOver7k'] = '';		
		$lead['LenderID'] = '';
		$lead['CallStatusID'] = '14';				

		// Setup cURL
		$data_string = json_encode($lead);
		$_posturl = 'http://applicationform.hatpacks.com.au/api/enquiryform/post';

		$ch = curl_init($_posturl);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string))
		);         	
		
		// Send the request
		$response = curl_exec($ch);		
		
		if ($response != FALSE) {
			
			// Decode the response
            $responseData = json_decode($response, TRUE);
			
			// Save lead ID in the session
			if ($responseData['status'] == 'SUCCESS') {								
				setcookie('lead_id', $responseData['ID'], time() + (86400 * 30), "/"); // 86400 = 1 day				
			}			
		}
			
	} else {
		
		$lead = array();
		$lead['comments'] = $comments;
		
		// Update Lead		
		if (isset($_COOKIE['lead_id'])) {
			
			$lead['CallID'] = $_COOKIE['lead_id'];		
			
			// Setup cURL
			$data_string = json_encode($lead);
			$_posturl = 'http://applicationform.hatpacks.com.au/api/enquiryform/addComment';

			$ch = curl_init($_posturl);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($data_string))
			); 

			// Send the request
			$response = curl_exec($ch);
		}
	}
	
	//die(var_dump(json_decode($response, TRUE)));	
	//mail('boni@chillidee.com.au', 'Budget Calculator - RESPONSE' , $response, $headers);

	echo '<script>window.close();</script>';

  } else {
    die('Ooops, there is no any data.');
  }
} else {
  die('Ooops, there is no any data.');
}