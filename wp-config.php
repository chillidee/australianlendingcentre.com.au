<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'admin_alc' );

/** MySQL database username */
define( 'DB_USER', 'admin_alc' );

/** MySQL database password */
define( 'DB_PASSWORD', '3Bz5XAKixo' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'y$f7l6{+YP2@<u_o(H3].?_ rX}G?c]`gF~<>ii-l@M^9BGMFd2P0Fb`p>%X{}2-');
define('SECURE_AUTH_KEY',  ',|wfLzgeQMdIo<LhU,6g(M&9|JHj-c?cV]|T39he1-_::i>5mZK-!=Jdk/HBz<72');
define('LOGGED_IN_KEY',    '+O:*=DwpH+y!;H*+fv=%h@E9fzm1 s0f}+-D#I#o l|r&]6RcQ2h|OK?a)+::^|a');
define('NONCE_KEY',        '#(6For(+F@+%s?(&J9N[cwdZYw$m)2/p5PFBg}L~Ekz.-gK?s-$i`O~y-JCAe)@6');
define('AUTH_SALT',        'R-1PS(K~4fiI`WI*X1Fg%Qd!VR#!G2h_v)Hg?H yeSm%-%HS:*9jRs!X)/E?-D:?');
define('SECURE_AUTH_SALT', '$C,v!=1+OC$%;[8kIS0@Z~rD]dr0=+3rX/H+:uZ4Cs;LM^GI6!8ss2[>f+p(Sx;%');
define('LOGGED_IN_SALT',   'b17Q~HxAMe|7lYPOF~xns&5ezQ>|>-UC9MtOi)M;~+e{~indvvq8lG[4<rhZo+.A');
define('NONCE_SALT',       '8G|L:+i~G,>Dyj5z!EkF0DaQcMP,m?G(ZLc3%+Qw +u}F=8.I2m9CDD55%h~6-Ko');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
